/*
Menu
    1. Template Literals
    2. Destructuring Objects
    3. Destructuring arrays
    4. Object literals

*/


// template literals
    let a = 'hello'
    let b = 'sam'
    console.log(`${a} ${b}`)
    console.log(`${a} 
    ${b}`)

//  Destructuring Objects
    
    // Object
    const person = {
        firstName: 'Sivanesh',
        lastName: 'S',
        city: 'Madurai',
    }

    const {firstName, lastName} = person
    // you can use your variables without objects
    console.log(`${firstName} ${lastName}`)

    // Reassign your own values
    const {firstName: name, lastName: initial} = person
    console.log(`${name} ${initial}`)

// Destructuring arrays
    let nameArray = ['Siv', 'Bhav', 'Eleven', ]

    let [me, them, stranger] = nameArray
    console.log(`${me} ${them} ${stranger}`)

    let [mee, ,strangere] = nameArray
    console.log(`${mee} ${strangere}`)

// Object literal
    // keys same means just add single
    let name2 = 'Sivanesh', age = 12;
    let obj1 = {name, age}
    console.log(obj1)


// CHALLENGE
    ranger({name: 'Jack', ranger:  'red', from: 'spd'})

    function ranger(obj) {
        const {name, ranger, from} = obj
        console.log(`${name} ${ranger} ${from}`)
    } 

// for of loop
    // just to iterate but not for assignment or updating
    let array1 = [62000, 31, 20, 30]
    let total = 0

    for(let a of array1) {
        total += a
    }
    console.log(total)

    // Iterable through string also
    let str = 'hello world'
    console.log(str[1])

    
    for(let a of str) a = '1'

// Spread operator

    let array2 = [1,2,3,4,5,6]
    let array3 = [10, ...array2]
    console.log(array3)
    array3.push(...array2)
    console.log(array3)

    let obj2 = {
        name: 'Sivanesh',
        age: 21
    }

    // Spread operator works in Objects too
    let obj3 = {...obj2}
    console.log(obj3)

// Rest operators
    // used in functions (if argument length is unpredictable)
    function add(...args) {
        let ans = 0
        for(let i of args) ans += i
        console.log(ans)
    }

    add(1,2,3,4,5,)

// Arrow functions
    let array4 = [1, 2, 3, 4, 5]
    console.log(array4.reduce((a, b) => a + b) )
    console.log(array4.reduce((a, b) => a + b, 10) )

// Default parameters
    function add2(arr = [1]) {
        console.log(arr)
    }
    add2()
    add2([3, 4, 5])

// includes
    let array5 = [1, 2, 3, 4, 5]
    // returns index (indirect method)
    console.log(array5.indexOf(0))
    // returns boolean 
    console.log(array5.includes(0))

// let and const  
    if(false) var example = 5
    console.log(example)

    /*
        let uses block scope
        Hence it get hoisted inside block only
    */

    /*
    if(false) let example1 = 5
    console.log(example1)
SyntaxError: lexical declarations can't appear in single-statement context
    */

    const array6 = []
    array6.push(4)
    // we can insert elements in a const array but not change them overall
    // it will be for objects too

// Import and Export
    // Improves modularity


// Padstart padEnd()
    // Just fillers
    let name3 = 'Sivanesh'
    console.log(name3.padStart(10, 'a'))
    console.log(name3.padEnd(10, 'a'))
    
    // if
    console.log(name3.padEnd(10))
    console.log(name3.padEnd(10).length)
    console.log(name3.padStart(100).length)

// classes
    class PersonClass {

        constructor(name = "Sivanesh") {
            this.name = name
        }

        static return10 () {return 10}

    }

    let personname = new PersonClass();
    console.log(personname)
    console.log(PersonClass.return10())

// Trailing commas
    // extra commas with no values mentioned also works
    let array7 = [1,2,3,]
    function sub(...args) {
        let tot = 0
        console.log(args.reduce((a, b) => a - b))
    }
    // works fine
    sub(100, 10, 2,)

// Promises AYSNC AWAIT

// Set
    // Unique list like Java Sets
    // Set is not an array but just looks like
    const set = new Set([1, 2, 5 ,2, 2, 3,3,2,1])
    console.log(set)
    // insert
    set.add(9).add(10)
    console.log(set, set.add(11))
    
    // delete
    console.log(set.delete(10), set)

    // contains
    console.log(set.has(5))

    //clear
    set.clear()
    console.log(set)

    // Iteration forEach, forOf can be used

